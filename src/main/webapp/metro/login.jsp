<%--
  Created by IntelliJ IDEA.
  User: mihai-catalin.glavan
  Date: 14-Jul-17
  Time: 11:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>LOGIN FORM</title>
    <link rel="stylesheet" type="text/css" href="../semantic/semantic.min.css">
    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    ></script>
    <script src="../semantic/semantic.min.js"></script>
    <script src="../js/validate.js"></script>
    <style type="text/css">
        body {
            background: url('../images/img4.jpg') no-repeat fixed center;
        }

        body > .grid {
            height: 100%;
        }

        .image {
            margin-top: -100px;
        }

        .column {
            max-width: 450px;
        }
    </style>
</head>
<body>
<div class="ui fixed black inverted menu">
    <div class="ui container">
        <img src="../images/ms.png">
        <a class="header item">
            Catalin Glavan
        </a>
        <a href="${pageContext.request.contextPath}/" class="item">Home</a>
        <div class="ui simple inverted black dropdown item">
            Menu <i class="dropdown icon"></i>
            <div class="menu">
                <a class="ui item" href="${pageContext.request.contextPath}/metro-login">Login</a>
                <a class="ui item" href="${pageContext.request.contextPath}/metro-register">Register</a>
            </div>
        </div>
    </div>
</div>

<div class="ui middle aligned center aligned grid">
    <div class="column">
        <div class="ui relaxed segment">
            <img src="../images/ms2.png" class="image">
            <h2 class="ui black image header">
                <div class="content">
                    Log-in to your account
                </div>
            </h2>
            <form class="ui large relaxed form" action="metro-login" method="post">
                <div class="field">
                    <div class="ui animated fade left icon input" data-inverted=""
                         data-tooltip="enter your email address" data-position="bottom left">
                        <i class="user icon"></i>
                        <input id="email" name="email" placeholder="E-mail address">
                    </div>
                </div>
                <div class="field">
                    <div class="ui animated fade left icon input" data-inverted=""
                         data-tooltip="enter your correct password" data-position="bottom left">
                        <i class="lock icon"></i>
                        <input id="password" type="password" name="password" placeholder="Password">
                    </div>
                </div>
                <div align="center">
                    <div class="field">
                        <div class="ui checkbox">
                            <input type="checkbox" title="box">
                            <label>Remember me to login every time</label>
                            <br/>
                        </div>
                    </div>
                </div>
                <div class="ui fluid black large submit animated fade button" onClick="{isValidate(this.parentNode)}">
                    <div class="visible content">Login</div>
                    <div class="hidden content">Have fun !</div>
                </div>
                <a href="${pageContext.request.contextPath}/metro-register">
                    <div class="ui fluid large submit animated fade button">
                        <div class="visible content">Don't have an account ? Sign Up</div>
                        <div class="hidden content">Make a good choice !</div>
                    </div>
                </a>
            </form>
        </div>
    </div>
</div>
<div class="ui small basic modal">
    <div class="ui icon header">
        <i class="protect icon"></i>
        Protect Message
    </div>
    <div class="content">
        <p align="center">Please complete the current fields to login !</p>
    </div>
    <div class="actions">
        <div class="ui green ok inverted button">
            <i class="checkmark icon"></i>
            Yes
        </div>
        <div class="ui red basic cancel inverted button">
            <i class="remove icon"></i>
            No
        </div>
    </div>
</div>
</body>
</html>
