<%--
  Created by IntelliJ IDEA.
  User: mihai-catalin.glavan
  Date: 14-Jul-17
  Time: 11:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>REGISTER FORM</title>
    <link rel="stylesheet" type="text/css" href="../semantic/semantic.min.css">
    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
    <script src="../semantic/semantic.min.js"></script>
    <script src="../js/validate.js"></script>
    <style type="text/css">
        body {
            background: url('../images/img4.jpg') no-repeat fixed center;
        }

        body > .grid {
            height: 100%;
        }

        .image {
            margin-top: -100px;
        }

        .column {
            max-width: 600px;
        }
    </style>
</head>
<body>
<div class="ui fixed black inverted menu">
    <div class="ui container">
        <img src="../images/ms.png">
        <a class="header item">
            Catalin Glavan
        </a>
        <a href="${pageContext.request.contextPath}/" class="item">Home</a>
        <div class="ui simple inverted black dropdown item">
            Menu <i class="dropdown icon"></i>
            <div class="menu">
                <a class="ui item" href="${pageContext.request.contextPath}/metro-login">Login</a>
                <a class="ui item" href="${pageContext.request.contextPath}/metro-register">Register</a>
            </div>
        </div>
    </div>
</div>

<div class="ui middle aligned center aligned grid">
    <div class="column">
        <div class="ui relaxed segment">
            <img src="../images/ms2.png" class="image">
            <h2 class="ui black image header">
                <div class="content">
                    Register new account
                </div>
            </h2>
            <form class="ui large relaxed form" action="metro-register" method="post">
                <div class="field">
                    <div class="three fields">
                        <div class="six wide field">
                            <div class="ui animated fade left icon input" data-inverted=""
                                 data-tooltip="enter your first name" data-position="bottom left">
                                <i class="user icon"></i>
                                <input id="firstName" type="text" name="firstName" placeholder="First Name">
                            </div>
                        </div>
                        <div class="six wide field">
                            <div class="ui animated fade left icon input" data-inverted=""
                                 data-tooltip="enter your middle name" data-position="bottom left">
                                <i class="user circle icon"></i>
                                <input id="middleName" type="text" name="middleName" placeholder="Middle Name">
                            </div>
                        </div>
                        <div class="six wide field">
                            <div class="ui animated fade left icon input" data-inverted=""
                                 data-tooltip="enter your last name" data-position="bottom left">
                                <i class="user circle outline icon"></i>
                                <input id="lastName" type="text" name="lastName" placeholder="Last Name">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="one fields">
                        <div class="sixteen wide field">
                            <div class="ui animated fade left icon input" data-inverted=""
                                 data-tooltip="enter your email address" data-position="bottom left">
                                <i class="mail icon"></i>
                                <input id="email" type="text" name="email" placeholder="E-mail address">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="two fields">
                        <div class="eleven wide field">
                            <div class="ui animated fade left icon input" data-inverted=""
                                 data-tooltip="enter your password" data-position="bottom left">
                                <i class="lock icon"></i>
                                <input id="password" type="password" name="password" placeholder="Password">
                            </div>
                        </div>
                        <div class="eleven wide field">
                            <div class="ui animated fade left icon input" data-inverted=""
                                 data-tooltip="re-enter your password" data-position="bottom left">
                                <i class="lock icon"></i>
                                <input id="repassword" type="password" name="repassword" placeholder="Re-Password">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="two fields">
                        <div class="eleven wide field">
                            <div class="ui animated fade left icon input" data-inverted=""
                                 data-tooltip="enter your street address" data-position="bottom left">
                                <i class="address book icon"></i>
                                <input id="address book icon" type="text" name="streetAddress"
                                       placeholder="Street address">
                            </div>
                        </div>
                        <div class="six wide field">
                            <div class="ui animated fade left icon input" data-inverted=""
                                 data-tooltip="enter your postal code" data-position="bottom left">
                                <i class="address book outline icon"></i>
                                <input id="postalCode" type="number" name="postalcode" placeholder="Postal Code">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="two fields">
                        <div class="five wide field">
                            <label>
                                <select class="ui search dropdown">
                                    <option value="">Country</option>
                                    <option value="AL">Alabama</option>
                                    <option value="AK">Alaska</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="AR">Arkansas</option>
                                    <option value="CA">California</option>
                                    <option value="CO">Colorado</option>
                                    <option value="CT">Connecticut</option>
                                    <option value="DE">Delaware</option>
                                    <option value="DC">District Of Columbia</option>
                                    <option value="FL">Florida</option>
                                    <option value="GA">Georgia</option>
                                    <option value="HI">Hawaii</option>
                                    <option value="ID">Idaho</option>
                                    <option value="IL">Illinois</option>
                                    <option value="IN">Indiana</option>
                                    <option value="IA">Iowa</option>
                                    <option value="KS">Kansas</option>
                                    <option value="KY">Kentucky</option>
                                    <option value="LA">Louisiana</option>
                                    <option value="ME">Maine</option>
                                    <option value="MD">Maryland</option>
                                    <option value="MA">Massachusetts</option>
                                    <option value="MI">Michigan</option>
                                    <option value="MN">Minnesota</option>
                                    <option value="MS">Mississippi</option>
                                    <option value="MO">Missouri</option>
                                    <option value="MT">Montana</option>
                                    <option value="NE">Nebraska</option>
                                    <option value="NV">Nevada</option>
                                    <option value="NH">New Hampshire</option>
                                    <option value="NJ">New Jersey</option>
                                    <option value="NM">New Mexico</option>
                                    <option value="NY">New York</option>
                                    <option value="NC">North Carolina</option>
                                    <option value="ND">North Dakota</option>
                                    <option value="OH">Ohio</option>
                                    <option value="OK">Oklahoma</option>
                                    <option value="OR">Oregon</option>
                                    <option value="PA">Pennsylvania</option>
                                    <option value="RI">Rhode Island</option>
                                    <option value="RO">Romania</option>
                                    <option value="SC">South Carolina</option>
                                    <option value="SD">South Dakota</option>
                                    <option value="TN">Tennessee</option>
                                    <option value="TX">Texas</option>
                                    <option value="UT">Utah</option>
                                    <option value="VT">Vermont</option>
                                    <option value="VA">Virginia</option>
                                    <option value="WA">Washington</option>
                                    <option value="WV">West Virginia</option>
                                    <option value="WI">Wisconsin</option>
                                    <option value="WY">Wyoming</option>
                                </select>
                            </label>
                        </div>
                        <div class="twelve wide field icon ">
                            <label>
                                <select class="ui search dropdown address book outline icon">
                                    <option>Gender</option>
                                    <option value="F">
                                        <Female>Female</Female>
                                    </option>
                                    <option value="M">
                                        <Male>Male</Male>
                                    </option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
                <div align="center">
                    <div class="field">
                        <div class="ui checkbox">
                            <input type="checkbox" title="box">
                            <label>I agree to the Terms and Conditions</label>
                            <br/>
                        </div>
                    </div>
                </div>
                <div class="ui fluid black large submit animated fade button"
                     onClick="{this.parentNode.submit()}">
                    <div class="visible content">Sign Up</div>
                    <div class="hidden content">SIGN UP</div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
