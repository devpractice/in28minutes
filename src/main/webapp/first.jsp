<%--
  Created by IntelliJ IDEA.
  User: mihai-catalin.glavan
  Date: 13-Jul-17
  Time: 11:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome !!!!</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="jumbotron">
        <form action="test" method="post">
            <div class="input-group">
                <h2 class="input-group-addon">Welcome to my page, <input placeholder='${setname}' value='${setname}'
                                                                         name="check"/></h2>
                <button class="btn btn-primary" type="submit">STEP 2</button>
            </div>
        </form>
    </div>
</div>
</body>
</html>
