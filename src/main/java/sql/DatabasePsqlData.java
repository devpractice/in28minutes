// * @author Catalin Glavan
package sql;

import javax.security.sasl.AuthenticationException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DatabasePsqlData extends DatabasePsql {

    public void createUser(String firstName, String lastName, String email, String password, String streetAddress) throws AuthenticationException {
        try {

            String query = "INSERT INTO public.users (firstname, lastname, email, password, streetaddress) VALUES (?, ?, ?, ?, ?)";
            PreparedStatement statement = getCon().prepareStatement(query);
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setString(3, email);
            statement.setString(4, password);
            statement.setString(5, streetAddress);
            statement.executeUpdate();

        } catch (SQLException ex) {
            throw new AuthenticationException("Register error !");
        }
    }

}
