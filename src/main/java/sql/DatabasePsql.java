package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Catalin Glavan on 16-Jul-17.
 */

public class DatabasePsql {

    private static Connection con;
    private static Statement st;
    private static String username;
    private static String password;
    private static String database;

    public void DatabasePsql(String user, String pass, String db) {

        try {
            DatabasePsql.username = user;
            DatabasePsql.password = pass;
            DatabasePsql.database = db;
            String driver = "org.postgresql.Driver";
            String url = "jdbc:postgresql://localhost:5432/" + database;
            Class.forName(driver);
            con = DriverManager.getConnection(url, username, password);
            st = con.createStatement();
            PreparedStatement create = con.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS users (" +
                            "  id SERIAL PRIMARY KEY," +
                            "  firstName VARCHAR(30) NOT NULL ," +
                            "  lastName VARCHAR(30) NOT NULL ," +
                            "  email VARCHAR(30) NOT NULL ," +
                            "  password VARCHAR(30) NOT NULL ," +
                            "  streetAddress VARCHAR(30) NOT NULL ," +
                            "  UNIQUE (email) " +
                            ");");
            create.executeUpdate();
            st.close();
            System.out.println("PostgreeSQL CONNECTED !");
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DatabasePsql.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Connection getCon() {
        return con;
    }
}
