

//@WebServlet(name="Nume servlet", urlPatterns = "calea catre servlet")
//@WebServlet(value="calea catre servlet")

//@Controller - definire metoda controller in Spring dispatcherServlet

//@Component contine ( @Repository, @Service, @Controller )

//@Autowired - dependency injection

//@RequestMapping (value="calea catre controller, method=RequestMethod.GET"- se declara in metoda pentru a seta o ruta web catre metoda curenta,
// RequestMethod.GET - tipul metodei

//@ResponseBody - pentru a returna valorea direct in body (DOM)
// daca nu exista ResponseBody cauta automat un View Resolver, se declara un bean direct in dispatcherServlet pentru a returna ca default pe o anumita ruta cu un prefix si suffix
/*  <bean
        class="org.springframework.web.servlet.view.InternalResourceViewResolver">
            <property name="prefix">
                <value>/WEB-INF/views/</value>
            </property>
             <property name="suffix">
                <value>.jsp</value>
            </property>
    </bean>
*/


// Log4J - pentru log amanuntit


// WAR package - este integrata in src/main/webapp