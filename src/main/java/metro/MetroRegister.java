package metro;

import sql.DatabasePsql;
import sql.DatabasePsqlData;

import javax.security.sasl.AuthenticationException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by mihai-catalin.glavan on 14-Jul-17.
 */
public class MetroRegister extends HttpServlet {

    private static DatabasePsql psql;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        psql = new DatabasePsql();
        psql.DatabasePsql("admin_web", "admin", "web_app");

        RequestDispatcher rd = request.getRequestDispatcher("/metro/register.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String firstName = request.getParameter("firstName");
        String lasName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String streetAddrress = request.getParameter("streetAddress");

        RequestDispatcher rd;
        try {
            DatabasePsqlData psqlData = new DatabasePsqlData();
            psqlData.createUser(firstName, lasName, email, password, streetAddrress);
        } catch (AuthenticationException e) {
            rd = request.getRequestDispatcher("/metro/register.jsp");
            rd.forward(request, response);
        }
        rd = request.getRequestDispatcher("/metro/login.jsp");
        rd.forward(request, response);
    }

}
