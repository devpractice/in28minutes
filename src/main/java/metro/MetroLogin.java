package metro;

import sql.DatabasePsql;
import sql.DatabasePsqlUser;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by mihai-catalin.glavan on 14-Jul-17.
 */


public class MetroLogin extends HttpServlet {


    private static DatabasePsql psql;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        psql = new DatabasePsql();
        psql.DatabasePsql("admin_web", "admin", "web_app");

        RequestDispatcher rd = request.getRequestDispatcher("/metro/login.jsp");
        rd.forward(request, response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String e = request.getParameter("email");
        String p = request.getParameter("password");

        DatabasePsqlUser psqlUser = new DatabasePsqlUser();

        if (psqlUser.selectUserValidate(e, p)) {
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            out.println("<html><head><title>WELCOME PAGE</title></head><body bgcolor='#000000'>");
            out.println("<h2><font color='#FFFFFF'>");
            out.println("Welcome, " + psqlUser.getLastName() + " " + psqlUser.getFirstName());
            out.println("<br/>");
            out.println(psqlUser.getE());
            out.println("<br/>");
            out.println(psqlUser.getStreetAddress());
            out.println("</font></h2>");
            out.println("</body></html>");
        } else {
            request.getRequestDispatcher("/metro/login.jsp").forward(request, response);
        }
    }
}