import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "TestServlet", urlPatterns = "/test")
public class TestServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String set = request.getParameter("check");
        request.setAttribute("setname", set);
        RequestDispatcher rd = request.getRequestDispatcher("/first.jsp");
        rd.forward(request, response);

        // PrintWriter out = response.getWriter();

        /*out.println("<html>");
        out.println("<head>");
        out.println("<title>Yahoo!!!!!!!!</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("My First Servlet");
        out.println("</body>");
        out.println("</html>");
    */
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher rd;
        String check = request.getParameter("check");
        System.out.println(check);
        if (check.equals("servlet")) {
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            out.println("<label>Welcome to my servlet !</label>");
        } else if (check.equals("cata")) {
            rd = request.getRequestDispatcher("/first2.html");
            rd.forward(request, response);
        } else {
            rd = request.getRequestDispatcher("/first.jsp");
            rd.forward(request, response);
        }
    }
}